/**
 * @fileoverview Main app component for Hook Clock application.  Contains all components for Hook Clock.
 */

// Core imports
import React, { useState, useEffect } from "react";
import "./App.css";

// MUI imports
import { makeStyles, TextField, Button, Grid } from "@material-ui/core";

// Icon imports
import Clock from "../Clock/Clock";

/**
 * Custom styles for the App component
 * @type {Object} Style object
 */
const useStyles = makeStyles((theme) => ({
  root: {
    margin: "2em",
  },
  config: {
    margin: "0 1em 2em 0",
  },
}));

/**
 * Main wrapper for Clock app
 *
 * @return {Component} App component
 */
export default function App() {
  const classes = useStyles();

  // For our "Local Time" clock, grab the current time as a temporary placeholder value.
  // Used to initialize clockList in useState hook
  let initialLocalTime = new Date().getTime();

  // Clocks
  /**
   * @type {[Array, Function]} Array of clock objects
   */
  const [clockList, setClockList] = useState([
    {
      id: "LocalTimeClock",
      name: "Local Time",
      visible: true,
      offset: 0,
      time: initialLocalTime,
    },
  ]);

  /**
   * @type {[Number, Function]} The current Unix timestamp time
   */
  const [time, setTime] = useState(new Date());

  // Form controls input values
  /**
   * @type {[String, Function]} A name value input by the user via the "clock-name" TextField. Passed to clock components
   * @default ""
   */
  const [name, setName] = useState("");

  /**
   * @type {[Number, Function]} A clock time offset value in hours input by the user via the "hour-offset" TextField. Passed to clock components
   * @default 0
   */
  const [offset, setOffset] = useState(0);

  /**
   * @type {[Object, Function]} An object to store the state of "hour-offset" for input validation
   * @default { error: false, helperText: "" }
   */
  const [offsetControls, setOffsetControls] = useState({
    error: false,
    helperText: "",
  });

  /**
   * Fires when "clock-name" TextField is updated, commits "clock-name" TextField value to state as `name`
   * @param {*} event
   */
  const onNameChange = (event) => setName(event.target.value);

  /**
   * Fires when "hour-offset" TextField is updated, validates "hour-offset" TextField value and commits to state at `offset`
   * @param {*} event
   */
  const onOffsetChange = (event) => {
    setOffset(event.target.value);
    if (event.target.value) {
      setOffsetControls({ error: false, helperText: "" });
    } else {
      setOffsetControls({ error: true, helperText: "Offset must be a number" });
    }
  };

  /**
   * Fires when form data is submitted, adds a new clock object to the `ClockList` array
   * @param {*} event
   */
  const handleSubmit = (event) => {
    event.preventDefault();
    const clockTime = new Date().getTime();
    const clockID = name + clockList.length + "_" + clockTime;

    // If there is invalid data in our Offset input field,
    // create the clock assuming an offset of zero hours
    setClockList(
      clockList.concat({
        id: clockID,
        name: name,
        visible: true,
        offset: offset ? offset : 0,
        time: clockTime,
      })
    );

    // Reset our input fields and reset any errors
    setName("");
    setOffset(0);
    setOffsetControls({ error: false, helperText: "" });
  };

  /**
   * Disables visibility on a clock object
   *
   * @param {string} clockID The ID of the clock to toggle visibility
   */
  const hideClock = (clockID) => {
    let tempClockList = clockList;

    for (let i = 0; i < tempClockList.length; ++i) {
      if (clockID === tempClockList[i]["id"]) {
        tempClockList[i]["visible"] = false;
      }
    }
    setClockList(tempClockList);
  };

  /**
   * Updates the state `time` value
   *
   * @returns {number} Current time in milliseconds as a Unix timestamp
   */
  function tick() {
    setTime(new Date().getTime());
  }

  /**
   * Update all clocks optionally with an offset every time `time` updates
   */
  useEffect(() => {
    let tempClockList = clockList;

    for (let i = 0; i < tempClockList.length; ++i) {
      tempClockList[i]["time"] =
        time + parseInt(tempClockList[i]["offset"] * 60 * 60 * 1000, 10);
    }
    setClockList(tempClockList);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [time]);

  /**
   * Starts the main timer on mount
   */
  useEffect(() => {
    setInterval(() => tick(), 1000);
  });

  return (
    <div className={classes.root}>
      <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <TextField
          className={classes.config}
          id="clock-name"
          label="Clock Name"
          variant="outlined"
          size="small"
          value={name}
          onChange={onNameChange}
        />
        <TextField
          className={classes.config}
          id="hour-offset"
          label="Hour Offset"
          type="number"
          variant="outlined"
          size="small"
          value={offset}
          onChange={onOffsetChange}
          error={offsetControls.error}
          helperText={offsetControls.helperText}
        />
        <Button className={classes.config} variant="contained" type="submit">
          Add Clock
        </Button>
      </form>
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="stretch"
        spacing={3}
        id="clockList"
      >
        {clockList
          .filter((clock) => clock.visible)
          .map((clock) => (
            <Grid item xs={12} md={6} xl={4} key={clock.id}>
              <Clock
                id={clock.id}
                name={clock.name}
                visible={clock.visible}
                offset={clock.offset}
                time={clock.time}
                hideClock={hideClock}
              />
            </Grid>
          ))}
      </Grid>
    </div>
  );
}
