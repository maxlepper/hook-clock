/**
 * @fileoverview Tests for App.js
 */

import React from "react";
import { render } from "enzyme";

import { BrowserRouter } from "react-router-dom";

import App from "./App";

it("should test", () => {});

it("should render the App", () => {
  const component = render(
    <BrowserRouter>
      <App />
    </BrowserRouter>
  );
  expect(component).toMatchSnapshot();
});
