/**
 * @fileoverview Tests for Clock.js
 */

import React from "react";
import { render } from "enzyme";

import { BrowserRouter } from "react-router-dom";

import Clock from "./Clock";

it("should test", () => {});

it("should render Clock component", () => {
  const component = render(
    <BrowserRouter>
      <Clock
        id={"TestClock"}
        name={"Test Clock"}
        visible={true}
        offset={0}
        time={0}
      />
    </BrowserRouter>
  );
  expect(component).toMatchSnapshot();
});
