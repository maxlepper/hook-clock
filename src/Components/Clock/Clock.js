/**
 * @fileoverview Clock component. Displays a name, current time and offset, and the date as a Grid Card.
 */

// Core imports
import React from "react";
import PropTypes from "prop-types";

// MUI imports
import {
  makeStyles,
  Grid,
  Card,
  CardHeader,
  CardContent,
  IconButton,
  Typography,
} from "@material-ui/core";

// Icon imports
import ClearIcon from "@material-ui/icons/Clear";

/**
 * Custom styles for the Clock component
 * @type {object} Style object
 */
const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
  },
  card: {
    textAlign: "center",
  },
}));

/**
 * Generates a clock component. Displays a name, current time and offset, and the date as a Grid Card.
 *
 * @param {string} id - The ID of the clock
 * @param {string} name - A label for the clock
 * @param {bool} visible - Flag to indicate if the clock should be rendered to the DOM
 * @param {number} offset - The number of hours to offset from the current local time (+/-)
 * @param {number} time - The current clock time in milliseconds expressed as Unix time
 * @param {function} hideClock(id) - Function to instruct the App to set the clock referenced by id to `visible: false`
 * @return {component} Clock component
 */
export default function Clock(props) {
  const classes = useStyles();

  const id = props.id;
  const name = props.name;
  let visible = props.visible;
  const offset = props.offset;
  let time = new Date(props.time);
  const hideClock = props.hideClock;

  // If `visible=false` then `null` is returned instead of the assembled Clock component
  return visible ? (
    <Grid item component={Card} className={classes.root}>
      {/* "X" button to hide the card, and optionally a `name`, if assigned */}
      <CardHeader
        action={
          <IconButton
            aria-label="close"
            onClick={() => {
              hideClock(id);
            }}
          >
            <ClearIcon />
          </IconButton>
        }
        title={name.length > 0 && <span>{name}</span>}
      />
      <CardContent className={classes.card}>
        <Typography variant="h2" component="h2">
          {/* Current time, optionally indicating the hour offset */}
          {time.toLocaleTimeString()}
          {parseInt(offset, 10) !== 0 && <span> ({offset})</span>}
        </Typography>
        <Typography variant="h5" component="h5">
          {/* The date, used to show dates ahead or behind the current local date */}
          {time.toDateString()}
        </Typography>
      </CardContent>
    </Grid>
  ) : null;
}

Clock.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  visible: PropTypes.bool.isRequired,
  offset: PropTypes.number.isRequired,
  time: PropTypes.number.isRequired,
  hideClock: PropTypes.func.isRequired,
};
